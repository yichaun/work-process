# work process

This is the work process of Jinlin Wu.

### 2018年12月25日

- 联系思腾合力公司，修服务器，193和187均存在内存条报错的问题，需要进一步排查内存条；

- 187服务器调用4卡就会卡死，VNC和SSH同时掉线，疑似内存条导致；

- 发现PCB做随机擦除的数据增广(Random erasing)的实验有助于性能提升，需要进一步做剥离实验验证结论；

`PCB`结果

|Mehtod|mAP|Rank-1|
|:---:|:----:|:---:|
|bset|78.5|93.5|


### 2018年 12月27日：
- 实现`warm-up`训练策略
改进以前的，直接从pytorch的`_LRScheduler`类继承`warm-up`代码实现，并且通过重置`MultiStepLR`的初始学习率，将`warm-up`和`MultiStepLR`组合使用，简化代码和逻辑

- PCB测试时， 特征是否正则化
|Mehtod|mAP|Rank-1|
|:---:|:----:|:---:|
|正则化|80.7|93.1|
|不用正则化|80.9|92.7|
是否正则化其实区别并不大，但从理论上来讲，为了避免scale对测试结果的影响，测试时应当进行正则化

- 正在跑的三组实验是：
    1. Am-softmax-512-warmlr(1:1 or 1:10)-no-dropout
    2.  Am-softmax-512-warmlr(1:1)-dropout(0.25,0.5, 0.75)
    3. Am-softmax-1024-warmlr(1:1)-dropout(0.25,0.5, 0.75)

********

### 2018年12月28日
- A-softmax和AM-softmax训练结果

学习率设置
`warm-up`: [5e-5, 1e-3], 20epoch
`norm`: [1e-4, 1e-5, 1e-6], [80, 120, 160]
`total epoch`: 180 

|Mehtod|Rank-1|
|:---:|:----:|
|cosface=emb512-drop0.75-margin0.0-scale14|86.7|
|cosface=emb512-drop0.75-margin0.1-scale14|87.1|
|cosface=emb512-drop0.75-margin0.0-scale14|82.5|

学习率设置
1：10 [88.7]


`存在问题`：
`Sphere loss`的代码应该和当前pytorch的版本存在兼容性的问题，导致loss不下降，并不work

- 准备提取Market1501的tracklet的特征模板，并聚类



### 2018年12月30日
- 改进使用`Adam`优化算法，有明显提升

|method|Rank1|mAP|
|:-----:|:----:|:----:|
|SGD|84.4|67.7|
|**Adam-norm**|91.4|78.7|
|Adam-no-norm|91.0|78.8|
|Adam-no-norm-last-conv-2x2|87.5|73.1|

加`norm`层和不加`norm`层区别并不太大，很有可能是初始化的影响导致差别


还没做的事情：
- 应该及时放弃cosface的调试，开始tracklet提取特征，使用聚类算法分析特征
- iccv2019 时间为2019.3.22， 差不多60多天，得抓紧，准备introduction和related work
- 调研LinkNet和Graph的工作，能否将 K-NN图 嵌入模型中进行训练 ？？

*******

### 2019年1月1日

目前要做的工作:

- 审稿 
- 开始准备iccv 2019的baseline
   - PCB 跨库 baseline
   - resent + softmax baseline（ICME的方法）
   - 使用更快的聚类方法加速

- 调研时空信息模型

- 将ICME的工作，增加一个idea写一篇期刊
*******

重新训练的模型跨库记录：
`Market-1501-Softmax-SGD`:

|M->M|M->D|M-C|M->C (CUHK03 benchmark)|
|:---:|:---:|:---:|:----------------:|
|91.3(77.4)|33.2(18.7)|14.9(6.9)|17.7(29.1)|


`DukeMTMC-Softmax-SGD`:

|D->D|D->M|D->C|D->C (CUHK03 benchmark)|
|:--:|:--:|:--:|:--------------------:|
|82.5(65.5)|45.3(19.2)|6.1(6.4)|15.5(28.3)|

`MSMT17-Softmax-SGD`:
|MS->MS|MS->M|MS->D|MS->C|MS->C (cuhk03 benchmark)|
|:----:|:---:|:---:|:---:|:---------------------:|
|64.5(32.6)|49.2(23.4)|53.4(31.9)|10.5(10.9)|41.6(28.0)|



#### `PCB`

`MSMT17-SGD-PCB` basline

|MS->MS|MS->M|MS->D|MS->C|MS->C (cuhk03 benchmark)|
|:----:|:---:|:---:|:---:|:---------------------:|
|70.8(44.1)|50.0(24.3)|48.2(29.6)|10.5(10.7)|27.3(41.7)|


`Market-1501-SGD-PCB` baseline

|M->M|M->MS|M->D|M->C|M->C (cuhk03 benchmark)|
|:--:|:---:|:--:|:--:|:--:|
|92.5(77.6)|5.6(1.7)|23.8(12.8)|2.9(3.2)|11.4(22.0)|

`DukeMTMC-reID-SGD-PCB` baseline

|D->D|D->M|D->MS|D->C|D->C (cuhk03 benchmark)|
|:--:|:--:|:---:|:--:|:--:|
|84.6(71.8)|47.5(22.3)|10.4(3.2)|5.8(5.7)|28.3(15.4)|

`Market-Adam-PCB` basline:

|M->M|M->MS|M->D|M->C|M->C (cuhk03 benchmark)|
|:--:|:---:|:--:|:--:|:--:|
|88.1(70.0)|-|8.7(17.1)|


`MSMT17-Adam-PCB` basline:
|MS->MS|MS->M|MS->D|MS->C|MS->C (cuhk03 benchmark)|
|:----:|:---:|:---:|:---:|:---------------------:|
|53.7(30.4)|30.7(12.7)|27.0(14.0)|


`DukeMTMC-reID-PCB` baseline:
|D->D|D->M|D->MS|D->C|D->C (cuhk03 benchmark)|
|:--:|:--:|:---:|:--:|:--:|
|79.4(64.2)|38.2(16.0)|



`分辨率统计`：
|Dataset|min|**mean**|max|
|:-----:|:-:|:--:|:-:|
|market1501|861|**8191**|8192|
|DukeMTMC-reID|2890|**18933**|92061|
|MSMT17|1620|**68024**|5263470|
|CUHK03|2850|**25184**|88580|

总结：
- 从实验结果来看，不出意外Adam的优化器明显效果不如SGD的好
- 可能清晰度和光照会影响reid库的能力，我猜测是否高清的图片训练的模型向低清domain迁移效果较好，低清向高清迁移，效果较差？？？


***************

### 2019年1月2日：


需要做的事：
- 准备single camera baseline
- 提取特征(Market1501和DukeMTMC)，使用聚类算法初步试探
- 看图的看论文
- 审稿[0/6]

已经做的事情：

- 准备Market-1501, DukeMTMC, MSMT17的单个摄像头的伪标签（CUHK03和这三个库的情况不一样，它的摄像头是10个，5对，因此还在处理中）

**数据说明**


|Dataset|Cam|total|train|test|query|gallery|name pattern|
|:-----:|:-:|:---:|:---:|:--:|:---:|:-----:|:--:|
|Market-1501|6|32668(1501)|12936(751)|19372(750)|3368|-|`0001_c1s1_001051_00.jpg`|
|DukeMTMC-reID|8|36411(1404)|16522(702)|17661 (702 person ID + 408 distrator ID) |2228(702)|-|` 0001_c2_f0046182.jpg`|
|MSMT17|15|126441(4101)|32621(1041)|93820(3060)|11659|82161|`0014_000_01_0303morning_0184_1.jpg`|

`Market`，`Duke`和`MSMT17`都是`ID-cam-xxxx`的模式


各个摄像头的ID分布:

`Market-1501`:
|cam1|cam2|cam3|cam4|cam5|cam6|
|:--:|:--:|:--:|:--:|:--:|:--:|
|652|541|694|241|576|558|

`DukeMTMC-reID`:
|cam1|cam2|cam3|cam4|cam5|cam6|cam7|cam8|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|404|378|201|165|218|348|217|265|

`MTMS17`:
|cam1|cam2|cam3|cam4|cam5|cam6|cam7|cam8|cam9|cam10|cam11|cam12|cam13|cam14|cam15|
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:---:|:---:|:---:|:---:|:---:|:---:|
|669|14|81|173|679|207|670|137|154|58|389|308|379|649|254|

### 2019年1月3日

- 完成单摄像头伪标签的制作
- 单摄像头baseline
- 

`CUHK03-SGD-PCB` baseline:
|C->C|C->M|C->D|C->MS|C->C (cuhk03 benchmark)|
|:--:|:--:|:--:|:---:|:---------------------:|
|60.6(57.8)|36.2(16.8)|14.2(8.0)|3.1(0.9) |91.8(95.0)|


`CUHK03-SGD-Softmax` baseline:
|C->C|C->M|C->D|C->MS|C->C (cuhk03 benchmark)|
|:--:|:--:|:--:|:---:|:---------------------:|
||52.5(24.6)|33.1(16.3)|12.3(3.5)|83.6(76.2)|

`Market-1501-sinfle-camera-751-class-1fc-clssifier`:
||mAP|Rank-1|
|:-:|:-:|:-:|
|random-sample-evely-751-1fc|10.3|26.5|
|random-sample-cam-class-1fc|12.0|28.9|







********



















  